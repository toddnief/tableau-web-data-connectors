(function() {
    var myConnector = tableau.makeConnector();

    myConnector.getSchema = function(schemaCallback) {
        var cols = [{
                id: "client_id",
                alias: "Client ID",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "email",
                alias: "Client Email",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "name",
                alias: "Client Name",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "membership_start_date",
                alias: "Membership Start Date",
                dataType: tableau.dataTypeEnum.date
            },
        ];

        var tableSchema = {
            id: "1st_month_memberships",
            alias: "1st Month Memberships",
            columns: cols
        };

        schemaCallback([tableSchema]);
    };

    myConnector.getData = function(table, doneCallback) {
        api_key = "keyXHtQsr4b7kysSh";
        base_id = "appnYlcqzXrQ2MnlN";

        var Airtable = require('airtable');
        var base = new Airtable({ apiKey: api_key }).base(base_id);
        tableData = [];


        base('1st Month Memberships').select().eachPage(function page(records, fetchNextPage) {
            records.forEach(function(record) {

                var client_id;
                if (record.fields["Email"] === null) {
                    client_id = null;
                } else if (typeof record.fields["Email"] === "undefined") {
                    client_id = null;
                } else {
                    client_id = record.fields["Email"][0];
                }

                tableData.push({
                    "client_id": client_id,
                    "email": record.fields["E-mail (Plain Text)"],
                    "name": record.fields["Name"],
                    "membership_start_date": record.fields["Membership Start Date"],
                });
            });
            table.appendRows(tableData);
            tableData = [];
            fetchNextPage();
        }, function done(error) {
            console.log(error);
            doneCallback();
        });
    };

    tableau.registerConnector(myConnector);
    $(document).ready(function() {
        $("#submitButton").click(function() {
            tableau.connectionName = "1st Month Memberships";
            tableau.submit();
        });
    });
})();