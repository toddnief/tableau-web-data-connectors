(function () {
    var myConnector = tableau.makeConnector();

    myConnector.getSchema = function (schemaCallback) {
    var cols = [
      {id: "class",
      alias: "Class",
      dataType: tableau.dataTypeEnum.string},
      {id: "name",
      alias: "Client Name",
      dataType: tableau.dataTypeEnum.string},
      {id: "date",
      alias: "Date & Time",
      dataType: tableau.dataTypeEnum.datetime},
      {id: "client_record_id",
      alias: "Client Record ID",
      dataType: tableau.dataTypeEnum.string},
      /*{id: "membership_start_date",
      alias: "Membership Start Date",
      dataType: tableau.dataTypeEnum.datetime},
      {id: "new_member_end",
      alias: "120 Days After Membership Start",
      dataType: tableau.dataTypeEnum.datetime},*/
      {id: "new_member_check_in",
      alias: "New Member Check-In",
      dataType: tableau.dataTypeEnum.bool},
    ];

    var tableSchema = {
        id: "slsc_attendance",
        alias: "SLSC Attendance",
        columns: cols
    };

    schemaCallback([tableSchema]);
};

    myConnector.getData = function(table, doneCallback) {
  api_key = "keyXHtQsr4b7kysSh";
  base_id = "appnYlcqzXrQ2MnlN";

  var Airtable = require('airtable');
  var base = new Airtable({ apiKey: api_key }).base(base_id);
        tableData = [];


  base('Attendance').select().eachPage(function page(records, fetchNextPage) {
    records.forEach(function(record) {
      //debugger
      /*var membership_start_date;
      var new_member_end;
      if (record.fields["Membership Start Date"] === null) {
        membership_start_date = null;
        new_member_end = null;
      }
      else if (typeof record.fields["Membership Start Date"] === "undefined") {
        membership_start_date = null;
        new_member_end = null;
      }
      else {
        membership_start_date = record.fields["Membership Start Date"][0];
        new_member_end = record.fields["120 Days After Membership Start"][0];
      }*/

      var new_member_check_in;
      if ((record.fields["New Member Check-in?"] != 0) && (record.fields["New Member Check-in?"] != 1)) {
        new_member_check_in = false;
      }
      else if (record.fields["New Member Check-in?"] == 0) {
        new_member_check_in = false;
       }
      else if (record.fields["New Member Check-in?"] == 1) {
         new_member_check_in = true;
      }

       //debugger

      tableData.push({
        "class": record.fields["Class"],
        "name": record.fields["Client Name"],
        "date": record.fields["Time"],
        //"membership_start_date": membership_start_date,
        //"new_member_end": new_member_end,
        "client_record_id": record["id"],
        "new_member_check_in": new_member_check_in,
      });
    });
    table.appendRows(tableData);
    fetchNextPage();
  }, function done(error) {
    console.log(error);
    doneCallback();
  });
};

    tableau.registerConnector(myConnector);
    $(document).ready(function () {
      $("#submitButton").click(function () {
        tableau.connectionName = "SLSC Attendance";
        tableau.submit();
      });
    });
})();
