(function() {
    var myConnector = tableau.makeConnector();

    myConnector.getSchema = function(schemaCallback) {
        var cols = [{
                id: "email",
                alias: "Email",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "name",
                alias: "Client Name",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "consult_outcome",
                alias: "Consult Outcome",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "consult_date",
                alias: "Consult Date",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "consult_coach",
                alias: "Consult Coach",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "consult_type",
                alias: "Consult Type",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "lead_source",
                alias: "Lead Source",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "objection",
                alias: "Objection",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "membership_type",
                alias: "Membership Type",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "client_record_id",
                alias: "Client Record ID",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "funnel_stage",
                alias: "Funnel Stage",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "primary_interest",
                alias: "Primary Interest",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "membership_start_date",
                alias: "Membership Start Date",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "trial_week_or_elements_start_date",
                alias: "Trial Week | Elements Start Date",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "outreach_date",
                alias: "Outreach Date",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "attendances_first_30_days",
                alias: "Attendances (First 30 Days)",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "elements_boolean",
                alias: "Elements? [Boolean]",
                dataType: tableau.dataTypeEnum.bool
            },
            {
                id: "trial_boolean",
                alias: "Trial? [Boolean]",
                dataType: tableau.dataTypeEnum.bool
            },
            {
                id: "pricing_request_boolean",
                alias: "Pricing Request? [Boolean]",
                dataType: tableau.dataTypeEnum.bool
            },
            {
                id: "consult_request_boolean",
                alias: "Consult Request? [Boolean]",
                dataType: tableau.dataTypeEnum.bool
            },
            {
                id: "earliest_acuity_appointment",
                alias: "Earliest Acuity Appointment",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "earliest_zp_bill",
                alias: "Earliest ZP Bill",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "earliest_membership_start_date",
                alias: "Earliest Membership Start Date",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "earliest_outreach",
                alias: "Earliest Outreach",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "manual_start_date",
                alias: "Start Date (Manual)",
                dataType: tableau.dataTypeEnum.date
            },
        ];

        var tableSchema = {
            id: "slsc_clients",
            alias: "SLSC Clients",
            columns: cols
        };

        schemaCallback([tableSchema]);
    };

    myConnector.getData = function(table, doneCallback) {
        api_key = "keyXHtQsr4b7kysSh";
        base_id = "appnYlcqzXrQ2MnlN";

        var Airtable = require('airtable');
        var base = new Airtable({ apiKey: api_key }).base(base_id);
        tableData = [];


        base('SLSC CRM').select().eachPage(function page(records, fetchNextPage) {
            records.forEach(function(record) {
                // debugger;

                var membership_type;
                if (record.fields["Membership Type"] === null) {
                    membership_type = null;
                } else if (typeof record.fields["Membership Type"] === "undefined") {
                    membership_type = null;
                } else {
                    membership_type = record.fields["Membership Type"][0];
                }

                tableData.push({
                    "email": record.fields["E-mail"],
                    "name": record.fields["Client Name"],
                    "consult_coach": record.fields["Consult Coach"],
                    "consult_outcome": record.fields["Consult Outcome"],
                    "consult_date": record.fields["Consult Date"],
                    "consult_type": record.fields["Consult Type"] ? record.fields["Consult Type"].join(", ") : "",
                    "lead_source": record.fields["Lead Source (Field)"],
                    "objection": record.fields["Objection"],
                    "membership_type": membership_type, //record.fields["Membership Type"] //? record.fields["Membership Type"].join(", ") : "",
                    "client_record_id": record["id"],
                    "funnel_stage": record.fields["Funnel Stage"],
                    "primary_interest": record.fields["Primary Interest"],
                    "membership_start_date": record.fields["Membership Start Date"],
                    "attendances_first_30_days": record.fields["Attendances (First 30 Days)"],
                    "outreach_date": record.fields["Outreach Date [Calculated]"],
                    "elements_boolean": record.fields["Elements? [Boolean]"],
                    "trial_boolean": record.fields["Trial? [Boolean]"],
                    "pricing_request_boolean": record.fields["Pricing Request? [Boolean]"],
                    "consult_request_boolean": record.fields["Consult Request? [Boolean]"],
                    "trial_week_or_elements_start_date": record.fields["Trial Week | Elements Start Date"],
                    "earliest_acuity_appointment": record.fields["Earliest Acuity Appointment"],
                    "earliest_zp_bill": record.fields["Earliest ZP Bill"],
                    "earliest_membership_start_date": record.fields["Earliest Membership Start Date"],
                    "earliest_outreach": record.fields["Earliest Outreach"],
                    "manual_start_date": record.fields["Start Date (Manual)"],
                });
            });
            table.appendRows(tableData);
            tableData = [];
            fetchNextPage();
        }, function done(error) {
            console.log(error);
            doneCallback();
        });
    };

    tableau.registerConnector(myConnector);
    $(document).ready(function() {
        $("#submitButton").click(function() {
            tableau.connectionName = "SLSC CRM";
            tableau.submit();
        });
    });
})();