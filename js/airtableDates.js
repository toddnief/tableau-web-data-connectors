(function() {
    var myConnector = tableau.makeConnector();

    myConnector.getSchema = function(schemaCallback) {
        var cols = [{
                id: "date",
                alias: "Date",
                dataType: tableau.dataTypeEnum.date
            },
        ];

        var tableSchema = {
            id: "date_scaffold",
            alias: "Date Scaffold",
            columns: cols
        };

        schemaCallback([tableSchema]);
    };

    myConnector.getData = function(table, doneCallback) {
        api_key = "keyXHtQsr4b7kysSh";
        base_id = "appnYlcqzXrQ2MnlN";

        var Airtable = require('airtable');
        var base = new Airtable({ apiKey: api_key }).base(base_id);
        tableData = [];


        base('Date Scaffold').select().eachPage(function page(records, fetchNextPage) {
            records.forEach(function(record) {

                tableData.push({
                    "date": record.fields["Date"],
                });
                //debugger
            });
            table.appendRows(tableData);
            tableData = [];
            fetchNextPage();
        }, function done(error) {
            console.log(error);
            doneCallback();
        });
    };

    tableau.registerConnector(myConnector);
    $(document).ready(function() {
        $("#submitButton").click(function() {
            tableau.connectionName = "Date Scaffold";
            tableau.submit();
        });
    });
})();