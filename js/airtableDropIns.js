(function() {
    var myConnector = tableau.makeConnector();

    myConnector.getSchema = function(schemaCallback) {
        var cols = [{
                id: "item",
                alias: "Item",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "name",
                alias: "Client Name or E-mail",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "date",
                alias: "Date",
                dataType: tableau.dataTypeEnum.datetime
            },
            {
                id: "source",
                alias: "Source",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "amount_paid",
                alias: "Amount Paid",
                dataType: tableau.dataTypeEnum.float
            },
            {
                id: "drop_in_record_id",
                alias: "Drop In Record ID",
                dataType: tableau.dataTypeEnum.string
            },
        ];

        var tableSchema = {
            id: "slsc_drop_ins",
            alias: "SLSC Drop-Ins",
            columns: cols
        };

        schemaCallback([tableSchema]);
    };

    myConnector.getData = function(table, doneCallback) {
        api_key = "keyXHtQsr4b7kysSh";
        base_id = "appnYlcqzXrQ2MnlN";

        var Airtable = require('airtable');
        var base = new Airtable({ apiKey: api_key }).base(base_id);
        tableData = [];


        base('Drop-Ins').select().eachPage(function page(records, fetchNextPage) {
            records.forEach(function(record) {
                tableData.push({
                    "item": record.fields["Item"],
                    "name": record.fields["E-mail or Name"],
                    "date": record.fields["Date"],
                    "source": record.fields["Source"],
                    "amount_paid": record.fields["Amount"],
                    "drop_in_record_id": record["id"],
                });
            });
            //debugger
            table.appendRows(tableData);
            fetchNextPage();
        }, function done(error) {
            console.log(error);
            doneCallback();
        });
    };

    tableau.registerConnector(myConnector);
    $(document).ready(function() {
        $("#submitButton").click(function() {
            tableau.connectionName = "SLSC Drop-Ins";
            tableau.submit();
        });
    });
})();