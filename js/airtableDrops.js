(function() {
    var myConnector = tableau.makeConnector();

    myConnector.getSchema = function(schemaCallback) {
        var cols = [{
                id: "drop_reason",
                alias: "Drop Reason",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "name",
                alias: "Client Name",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "client_record_id",
                alias: "Client Record ID",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "drop_date",
                alias: "Drop Date",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "drop_month",
                alias: "Drop Month",
                dataType: tableau.dataTypeEnum.date
            },
        ];

        var tableSchema = {
            id: "dropped_memberships",
            alias: "Dropped Memberships",
            columns: cols
        };

        schemaCallback([tableSchema]);
    };

    myConnector.getData = function(table, doneCallback) {
        api_key = "keyXHtQsr4b7kysSh";
        base_id = "appnYlcqzXrQ2MnlN";

        var Airtable = require('airtable');
        var base = new Airtable({ apiKey: api_key }).base(base_id);
        tableData = [];


        base('Dropped Memberships').select().eachPage(function page(records, fetchNextPage) {
            records.forEach(function(record) {
                //debugger
                var client_id;
                if (record.fields["Drop E-mail"] === null) {
                    client_id = null;
                } else if (typeof record.fields["Drop E-mail"] === "undefined") {
                    client_id = null;
                } else {
                    client_id = record.fields["Drop E-mail"][0];
                }

                tableData.push({
                    "drop_reason": record.fields["Drop Reason"],
                    "name": record.fields["Name"],
                    "drop_date": record.fields["Drop Date"],
                    "drop_month": record.fields["Drop Month"],
                    "client_record_id": client_id,
                });
            });
            table.appendRows(tableData);
            tableData = [];
            fetchNextPage();
        }, function done(error) {
            console.log(error);
            doneCallback();
        });
    };

    tableau.registerConnector(myConnector);
    $(document).ready(function() {
        $("#submitButton").click(function() {
            tableau.connectionName = "Dropped Memberships";
            tableau.submit();
        });
    });
})();