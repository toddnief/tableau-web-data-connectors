(function() {
    var myConnector = tableau.makeConnector();

    myConnector.getSchema = function(schemaCallback) {
        var cols = [{
                id: "hold_reason",
                alias: "Hold Reason",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "name",
                alias: "Client Name",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "client_record_id",
                alias: "Client Record ID",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "hold_start_date",
                alias: "Hold Start Date",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "hold_end_date",
                alias: "Hold End Date",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "hold_active_date",
                alias: "Hold Active Date",
                dataType: tableau.dataTypeEnum.date
            },
        ];

        var tableSchema = {
            id: "membership_holds",
            alias: "Membership Holds",
            columns: cols
        };

        schemaCallback([tableSchema]);
    };

    myConnector.getData = function(table, doneCallback) {
        api_key = "keyXHtQsr4b7kysSh";
        base_id = "appnYlcqzXrQ2MnlN";

        var Airtable = require('airtable');
        var base = new Airtable({ apiKey: api_key }).base(base_id);
        tableData = [];


        base('Membership Holds').select().eachPage(function page(records, fetchNextPage) {
            records.forEach(function(record) {

                var client_id;
                if (record.fields["Hold E-mail"] === null) {
                    client_id = null;
                } else if (typeof record.fields["Hold E-mail"] === "undefined") {
                    client_id = null;
                } else {
                    client_id = record.fields["Hold E-mail"][0];
                }

                tableData.push({
                    "hold_reason": record.fields["Hold Reason"],
                    "name": record.fields["Name"],
                    "client_record_id": client_id,
                    "hold_start_date": record.fields["Hold Start Date"],
                    "hold_end_date": record.fields["Hold End Date"],
                    "hold_active_date": record.fields["Hold Active Date"],
                });
                //debugger
            });
            table.appendRows(tableData);
            tableData = [];
            fetchNextPage();
        }, function done(error) {
            console.log(error);
            doneCallback();
        });
    };

    tableau.registerConnector(myConnector);
    $(document).ready(function() {
        $("#submitButton").click(function() {
            tableau.connectionName = "Membership Holds";
            tableau.submit();
        });
    });
})();