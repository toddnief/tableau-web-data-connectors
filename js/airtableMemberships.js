(function() {
    var myConnector = tableau.makeConnector();

    myConnector.getSchema = function(schemaCallback) {
        var cols = [{
                id: "membership_record_id",
                alias: "Membership Record ID",
                dataType: tableau.dataTypeEnum.string
            }, {
                id: "membership_type",
                alias: "Membership Type",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "name",
                alias: "Client Name",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "membership_create_date",
                alias: "Membership Create Date",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "client_record_id",
                alias: "Client Record ID",
                dataType: tableau.dataTypeEnum.string
            },
        ];

        var tableSchema = {
            id: "slsc_memberships",
            alias: "SLSC Memberships",
            columns: cols
        };

        schemaCallback([tableSchema]);
    };

    myConnector.getData = function(table, doneCallback) {
        api_key = "keyXHtQsr4b7kysSh";
        base_id = "appnYlcqzXrQ2MnlN";

        var Airtable = require('airtable');
        var base = new Airtable({ apiKey: api_key }).base(base_id);
        tableData = [];


        base('ZP Memberships').select().eachPage(function page(records, fetchNextPage) {
            records.forEach(function(record) {
                //debugger;

                var client_id;
                if (record.fields["E-mail"] === null) {
                    client_id = null;
                } else if (typeof record.fields["E-mail"] === "undefined") {
                    client_id = null;
                } else {
                    client_id = record.fields["E-mail"][0];
                }

                tableData.push({
                    "membership_record_id": record["id"],
                    "membership_type": record.fields["Membership Type"],
                    "name": record.fields["Member Name"],
                    "membership_create_date": record.fields["Membership Create Date"],
                    "client_record_id": client_id,
                });
            });
            table.appendRows(tableData);
            tableData = [];
            fetchNextPage();
        }, function done(error) {
            console.log(error);
            doneCallback();
        });
    };

    tableau.registerConnector(myConnector);
    $(document).ready(function() {
        $("#submitButton").click(function() {
            tableau.connectionName = "SLSC Memberships";
            tableau.submit();
        });
    });
})();