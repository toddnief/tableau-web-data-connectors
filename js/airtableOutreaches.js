(function () {
    var myConnector = tableau.makeConnector();

    myConnector.getSchema = function (schemaCallback) {
    var cols = [
      {id: "name",
      alias: "Client Name",
      dataType: tableau.dataTypeEnum.string},
      {id: "client_record_id",
      alias: "Client Record ID",
      dataType: tableau.dataTypeEnum.string},
      {id: "outreach_date",
      alias: "Outreach Date",
      dataType: tableau.dataTypeEnum.date},
      {id: "outreach_type",
      alias: "Outreach Type",
      dataType: tableau.dataTypeEnum.string},
      {id: "outreach_page",
      alias: "Outreach Page",
      dataType: tableau.dataTypeEnum.string},
      {id: "spam",
      alias: "Spam",
      dataType: tableau.dataTypeEnum.bool},
    ];

    var tableSchema = {
        id: "outreaches",
        alias: "SLSC Outreaches",
        columns: cols
    };

    schemaCallback([tableSchema]);
};

    myConnector.getData = function(table, doneCallback) {
  api_key = "keyXHtQsr4b7kysSh";
  base_id = "appnYlcqzXrQ2MnlN";

  var Airtable = require('airtable');
  var base = new Airtable({ apiKey: api_key }).base(base_id);
        tableData = [];


  base('Outreaches').select().eachPage(function page(records, fetchNextPage) {
    records.forEach(function(record) {

      var client_id;
      if (record.fields["Outreach Email"] === null) {
        client_record_id = null;
      }
      else if (typeof record.fields["Outreach Email"] === "undefined") {
        client_record_id = null;
      }
      else {
        client_record_id = record.fields["Outreach Email"][0];
      }

      tableData.push({
        "name": record.fields["Name"],
        "outreach_date": record.fields["Date"],
        "outreach_type": record.fields["Outreach Type"],
        "outreach_page": record.fields["Page"],
        "spam": record.fields["Spam"],
        "client_record_id": client_record_id,
      });
      //debugger
    });
    table.appendRows(tableData);
    tableData = [];
    fetchNextPage();
  }, function done(error) {
    console.log(error);
    doneCallback();
  });
};

    tableau.registerConnector(myConnector);
    $(document).ready(function () {
      $("#submitButton").click(function () {
        tableau.connectionName = "SLSC Outreaches";
        tableau.submit();
      });
    });
})();
