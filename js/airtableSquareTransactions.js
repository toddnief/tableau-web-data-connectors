(function() {
    var myConnector = tableau.makeConnector();

    myConnector.getSchema = function(schemaCallback) {
        var cols = [{
                id: "email",
                alias: "Email",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "transaction_date",
                alias: "Transaction Date",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "transaction_amount",
                alias: "Transaction Amount",
                dataType: tableau.dataTypeEnum.float
            }
        ];

        var tableSchema = {
            id: "square_transactions",
            alias: "SLSC Square Transactions",
            columns: cols
        };

        schemaCallback([tableSchema]);
    };

    myConnector.getData = function(table, doneCallback) {
        api_key = "keyXHtQsr4b7kysSh";
        base_id = "appnYlcqzXrQ2MnlN";

        var Airtable = require('airtable');
        var base = new Airtable({ apiKey: api_key }).base(base_id);
        tableData = [];


        base('SLSC Client Transactions (Square)').select().eachPage(function page(records, fetchNextPage) {
            records.forEach(function(record) {
                tableData.push({
                    "email": record.fields["Email (Plain Text)"],
                    "transaction_date": record.fields["Transaction Date"],
                    "transaction_amount": record.fields["Amount"],
                });
            });
            //debugger
            table.appendRows(tableData);
            fetchNextPage();
        }, function done(error) {
            console.log(error);
            doneCallback();
        });
    };

    tableau.registerConnector(myConnector);
    $(document).ready(function() {
        $("#submitButton").click(function() {
            tableau.connectionName = "SLSC Square Transactions";
            tableau.submit();
        });
    });
})();