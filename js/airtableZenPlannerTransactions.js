(function() {
    var myConnector = tableau.makeConnector();

    myConnector.getSchema = function(schemaCallback) {
        var cols = [{
                id: "bill_record_id",
                alias: "Bill Record ID",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "bill_description",
                alias: "Bill Description",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "client_id",
                alias: "Client ID",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "email",
                alias: "Client Email",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "status",
                alias: "Payment Status",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "paid_date",
                alias: "Paid Date",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "due_date",
                alias: "Due Date",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "paid_month",
                alias: "Paid Month",
                dataType: tableau.dataTypeEnum.date
            },
            {
                id: "due_month",
                alias: "Due Month",
                dataType: tableau.dataTypeEnum.date
            },

            {
                id: "amount_due",
                alias: "Amount Due",
                dataType: tableau.dataTypeEnum.float
            },
            {
                id: "real_revenue",
                alias: "Real Revenue",
                dataType: tableau.dataTypeEnum.float
            },
            {
                id: "personalized_services",
                alias: "Personalized Services?",
                dataType: tableau.dataTypeEnum.bool
            },
            {
                id: "client_retained",
                alias: "Client Retained?",
                dataType: tableau.dataTypeEnum.bool
            },
        ];

        var tableSchema = {
            id: "zen_planner_payments",
            alias: "Zen Planner Payments",
            columns: cols
        };

        schemaCallback([tableSchema]);
    };

    myConnector.getData = function(table, doneCallback) {
        api_key = "keyXHtQsr4b7kysSh";
        base_id = "appnYlcqzXrQ2MnlN";

        var Airtable = require('airtable');
        var base = new Airtable({ apiKey: api_key }).base(base_id);
        tableData = [];


        base('SLSC Client Transactions (ZP)').select().eachPage(function page(records, fetchNextPage) {
            records.forEach(function(record) {

            	//debugger;

                var client_id;
                if (record.fields["E-mail"] === null) {
                    client_id = null;
                } else if (typeof record.fields["E-mail"] === "undefined") {
                    client_id = null;
                } else {
                    client_id = record.fields["E-mail"][0];
                }

                tableData.push({
                    "bill_record_id": record["id"],
                    "bill_description": record.fields["Bill Description"],
                    "client_id": client_id,
                    "email": record.fields["E-mail (Plain Text)"],
                    "status": record.fields["Status"],
                    "paid_date": record.fields["Paid Date"],
                    "due_date": record.fields["Due Date"],
                    "paid_month": record.fields["Paid Month"],
                    "due_month": record.fields["Due Month"],
                    "amount_due": record.fields["Amount Due"],
                    "real_revenue": record.fields["Real Revenue"],
                    "personalized_services": record.fields["Personalized Services?"],
                    "client_retained": record.fields["Client Retained?"],
                });
            });
            table.appendRows(tableData);
            tableData = [];
            fetchNextPage();
        }, function done(error) {
            console.log(error);
            doneCallback();
        });
    };

    tableau.registerConnector(myConnector);
    $(document).ready(function() {
        $("#submitButton").click(function() {
            tableau.connectionName = "Zen Planner Transactions";
            tableau.submit();
        });
    });
})();